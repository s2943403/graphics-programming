#include <stdlib.h>			//- for exit()
#include <stdio.h>			//- for sprintf()
#include <string.h>			//- for memset()
#include <math.h>
#include <vector>
#include <iostream>
using namespace std;

#ifdef _WIN32
	#include "libs/glut.h"
	#include <windows.h>
	#pragma comment(lib, "winmm.lib")			//- not required but have it in anyway
	#pragma comment(lib, "libs/glut32.lib")
#elif __APPLE__
	#include <GLUT/glut.h>
#elif __unix__		// all unices including  __linux__
	#include <GL/glut.h>
#endif

//====== Macros and Defines =========

#define FRAME_WIDE	1000
#define FRAME_HIGH	600
#define ROUND(x) ((int)(x+0.5))
#define PI 3.14159265
#define RED {255, 0, 0}
#define GREEN {0, 255, 0}
#define BLUE {0, 0, 255}
#define WHITE {255, 255, 255}

//====== Structs & typedefs =========
typedef unsigned char BYTE;
struct POINT2D {int x, y;};
struct RGB { int r, g, b;};
typedef struct {
	int xL, xR;
	RGB cL, cR;
} EDGE_LIST;

//====== Global Variables ==========
BYTE	pFrameL[FRAME_WIDE * FRAME_HIGH * 3];
BYTE	pFrameR[FRAME_WIDE * FRAME_HIGH * 3];
int		shade = 0;
POINT2D	xypos = {0,0};
int		stereo = 0;
int		eyes = 10;

//===== Forward Declarations ========
void ClearScreen();
void DrawFrame();
void Interlace(BYTE* pL, BYTE* pR);
void PlaySoundEffect(char * filename);
void BuildFrame(BYTE *pFrame, int view);
void OnIdle(void);
void OnDisplay(void);
void reshape(int w, int h);
void OnMouse(int button, int state, int x, int y);
void OnKeypress(unsigned char key, int x, int y);

////////////////////////////////////////////////////////
// Program Entry Poinr
////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
	//-- setup GLUT --
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);	//GLUT_3_2_CORE_PROFILE |
	glutInitWindowSize(FRAME_WIDE, FRAME_HIGH);
	glutInitWindowPosition(100, 100);
	glutCreateWindow(argv[0]);
/*
#ifdef WIN32
	//- eliminate flashing --
	typedef void (APIENTRY * PFNWGLEXTSWAPCONTROLPROC) (int i);
	PFNWGLEXTSWAPCONTROLPROC wglSwapControl = NULL;
	wglSwapControl = (PFNWGLEXTSWAPCONTROLPROC) wglGetProcAddress("wglSwapIntervalEXT");
	if (wglSwapControl != NULL) wglSwapControl(1); 
#endif
*/

	//--- set openGL state --
	glClearColor (0.0, 0.0, 0.0, 0.0);
	glShadeModel(GL_FLAT);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	//-- register call back functions --
	glutIdleFunc(OnIdle);
	glutDisplayFunc(OnDisplay);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(OnKeypress);
	glutMouseFunc(OnMouse);

	//-- run the program
	glutMainLoop();
	return 0;
}


////////////////////////////////////////////////////////
// Event Handers
////////////////////////////////////////////////////////
      
void OnIdle(void)
{
	DrawFrame();
	glutPostRedisplay();
}


void OnDisplay(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glRasterPos2i(0, 0);
	glDrawPixels(FRAME_WIDE, FRAME_HIGH, GL_RGB,GL_UNSIGNED_BYTE, (GLubyte*)pFrameR);
	glutSwapBuffers();
	glFlush();
}

void reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, (GLdouble) w, 0.0, (GLdouble) h);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


void OnMouse(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
	{
		PlaySoundEffect("Laser.wav"); 
		if (++shade > 16) shade = 0;	
	}
}

void OnKeypress(unsigned char key, int x, int y)
{
	switch (key) 
	{ 
	case ' ': xypos.x = xypos.y = 0; break;
	case 's': stereo ^= 1, eyes = 10;break;
	case ']': eyes++;	break;
	case '[': eyes--;	break;
	case 27 : exit(0);
	}
	PlaySoundEffect("Whoosh.wav"); 
}


////////////////////////////////////////////////////////
// Utility Functions
////////////////////////////////////////////////////////


void ClearScreen()
{
	memset(pFrameL, 0, FRAME_WIDE * FRAME_HIGH * 3);
	memset(pFrameR, 0, FRAME_WIDE * FRAME_HIGH * 3);
}


void Interlace(BYTE* pL, BYTE* pR)
{
	int rowlen = 3 * FRAME_WIDE;
	for (int y = 0; y < FRAME_HIGH; y+=2)
	{
		for (int x = 0; x < rowlen; x++) *pR++ = *pL++;
		pL += rowlen;
		pR += rowlen;
	}
}


void DrawFrame()
{
	ClearScreen();
	
	if (!stereo) BuildFrame(pFrameR, 0);
	else {
		BuildFrame(pFrameL, -eyes);
		BuildFrame(pFrameR, +eyes);
		Interlace((BYTE*)pFrameL, (BYTE*)pFrameR);
	}
}

void	PlaySoundEffect(char * filename)		
{
#ifdef _WIN32
	PlaySound(filename, NULL, SND_ASYNC | SND_FILENAME ); 
#else
	char command[80];
	#ifdef __APPLE__
		sprintf(command, "afplay %s &", filename);
	#else
		sprintf(command, "play %s &", filename);
	#endif	
	system(command);
#endif
}

////////////////////////////////////////////////////////
// Drawing Function
////////////////////////////////////////////////////////

inline int interpolate (int startValue, int endValue, int step, int lastStep)
{
	return (endValue - startValue) * step / lastStep + startValue;
}

void SetPixel(BYTE *screen, int x, int y, int r, int g, int b)
{
	int index = (y * FRAME_WIDE + x) * 3;

	screen[index] = r;
	screen[index + 1] = g;
	screen[index + 2] = b;
}

void LineDDA(BYTE *screen, POINT2D pos, POINT2D pos1, RGB color)
{
	int dx = pos1.x - pos.x;
	int dy = pos1.y - pos.y;
	int steps;
	if (abs(dx) > abs(dy)) steps = abs(dx);
	else steps = abs(dy);
	double x_inc = dx / (double)steps;
	double y_inc = dy / (double)steps;
	double x = pos.x;
	double y = pos.y;
	SetPixel(screen, ROUND(x), ROUND(y), color.r, color.g, color.b);
	for (int i = 0; i<steps; i++, x += x_inc, y += y_inc)
		SetPixel(screen, ROUND(x), ROUND(y), color.r, color.g, color.b);
}

void LineDDAGradient(BYTE *screen, int x1, int y1, int x2, int y2, int startR, int startG, int startB, int endR, int endG, int endB)
{
	int r = startR;
	int g = startG;
	int b = startB;
	int dx = x2 - x1;
	int dy = y2 - y1;
	int steps;
	if (abs(dx) > abs(dy)) steps = abs(dx);
	else steps = abs(dy);
	double x_inc = dx / (double)steps;
	double y_inc = dy / (double)steps;
	double x = x1;
	double y = y1;
	SetPixel(screen, ROUND(x), ROUND(y), r, g, b);
	for (int i = 0; i < steps; i++, x += x_inc, y += y_inc)
	{
		r = interpolate(startR, endR, i, steps);
		g = interpolate(startG, endG, i, steps);
		b = interpolate(startB, endB, i, steps);
		SetPixel(screen, ROUND(x), ROUND(y), r, g, b);
	}
}

void FilledPolygon(BYTE *screen, POINT2D p, POINT2D p1, POINT2D p2, RGB color, RGB color1, RGB color2)
{
	double y1 = min(min(p.y, p1.y), p2.y);
	double x1, y2, x2, y3, x3;
	
	if (y1 == p.y)
	{
		x1 = p.x;
		y2 = p1.y;
		x2 = p1.x;
		y3 = p2.y;
		x3 = p2.x;
	}
	else if (y1 == p1.y)
	{
		x1 = p1.x;
		y2 = p.y;
		x2 = p.x;
		y3 = p2.y;
		x3 = p2.x;

		RGB temp = color1;
		color1 = color;
		color = temp;
	}
	else
	{
		x1 = p2.x;
		y2 = p1.y;
		x2 = p1.x;
		y3 = p.y;
		x3 = p.x;

		RGB temp = color;
		color = color2;
		color2 = temp;
	}

	double ang1 = atan2(y2 - y1, x2 - x1) * 180 / PI;
	double ang2 = atan2(y3 - y1, x3 - x1) * 180 / PI;

	//Swaps the lines so that the left line is always from x1, y1 to x2, y2
	if (ang1 < ang2 && ang1 != 0)
	{
		int temp = y3;
		y3 = y2;
		y2 = temp;
		temp = x3;
		x3 = x2;
		x2 = temp;

		RGB tempC = color1;
		color1 = color2;
		color2 = tempC;
	}
	
	double mL = (x2 - x1) / (double)(y2 - y1);
	double mR = (x3 - x1) / (double)(y3 - y1);
	double xL = x1;
	double xR = x1;

	//Colour Gradients
	double mRedL = (color1.r - color.r) / (double)(y2 - y1);
	double mGreenL = (color1.g - color.g) / (double)(y2 - y1);
	double mBlueL = (color1.b - color.b) / (double)(y2 - y1);
	double mRedR = (color2.r - color.r) / (double)(y3 - y1);
	double mGreenR = (color2.g - color.g) / (double)(y3 - y1);
	double mBlueR = (color2.b - color.b) / (double)(y3 - y1);

	//Left and right colours
	double redL = color.r;
	double greenL = color.g;
	double blueL = color.b;
	double redR = color.r;
	double greenR = color.g;
	double blueR = color.b;

	int hL = y2 - y1;
	int hR = y3 - y1;
	int high;
	if (hL > hR)
	{
		high = hL;
	}
	else
	{
		high = hR;

		if (hL == 0)
		{
			if (x1 > x2)
			{
				int temp = x2;
				x2 = x1;
				x1 = temp;
				xL = x1;
			}

			mL = (x3 - x1) / (double)(y3 - y1);
			mRedL = (color2.r - color.r) / (double)(y3 - y1);
			mGreenL = (color2.g - color.g) / (double)(y3 - y1);
			mBlueL = (color2.b - color.b) / (double)(y3 - y1);

			mR = (x3 - x2) / (double)(y3 - y2);
			mRedR = (color2.r - color1.r) / (double)(y3 - y2);
			mGreenR = (color2.g - color1.g) / (double)(y3 - y2);
			mBlueR = (color2.b - color1.b) / (double)(y3 - y2);

			xR = x2;

			redR = color1.r;
			greenR = color1.g;
			blueR = color1.b;

			hL = -1;
		}
	}

	for (int y = 0; y < high; y++)
	{
		int len = ROUND(xR) - ROUND(xL);
		double mRed = (len == 0) ? 0 : (redR - redL) / (double)len;
		double mGreen = (len == 0) ? 0 : (greenR - greenL) / (double)len;
		double mBlue = (len == 0) ? 0 : (blueR - blueL) / (double)len;

		double red = redL;
		double green = greenL;
		double blue = blueL;

		for (int x = 0; x <= len; x++) {
			SetPixel(screen, x + ROUND(xL), y + ROUND(y1), ROUND(red), ROUND(green), ROUND(blue));
			red += mRed;
			green += mGreen;
			blue += mBlue;
		}

		if (y == hL)
		{
			mL = (x3 - x2) / (double)(y3 - y2);
			mRedL = (color2.r - color1.r) / (double)(y3 - y2);
			mGreenL = (color2.g - color1.g) / (double)(y3 - y2);
			mBlueL = (color2.b - color1.b) / (double)(y3 - y2);
		}
		if (y == hR)
		{
			mR = (x2 - x3) / (double)(y2 - y3);
			mRedR = (color1.r - color2.r) / (double)(y2 - y3);
			mGreenR = (color1.g - color2.g) / (double)(y2 - y3);
			mBlueR = (color1.b - color2.b) / (double)(y2 - y3);
		}

		xL += mL;
		xR += mR;
		redL += mRedL;
		greenL += mGreenL;
		blueL += mBlueL;
		redR += mRedR;
		greenR += mGreenR;
		blueR += mBlueR;
	}

	/*LineDDA(screen, p, p1, RGB{255, 255, 255});
	LineDDA(screen, p, p2, RGB{ 255, 255, 255 });
	LineDDA(screen, p1, p2, RGB{ 255, 255, 255 });*/
}

bool ClipTest(double p, double q, double* u1, double* u2)
{
	double r = q / p;
	if(p < 0.0)
	{
		if (r > *u2) return false;
		else if (r > *u1) *u1 = r;
	}
	else if (p > 0.0)
	{
		if (r < *u1) return false;
		else if (r < *u2) *u2 = r;
	}
	//-- p == 0 so line is parallel
	//-- to clip boundary
	else if (q < 0.0) return false;
	return true;
}

void ClipLine(BYTE * screen, RECT clip, POINT2D p1, POINT2D p2)
{
	double t1 = 0.0, t2 = 1.0;
	double dy = p2.y - p1.y, dx = p2.x - p1.x;
	if (ClipTest(-dx, p1.x - clip.left, &t1, &t2))
		if (ClipTest(dx, clip.right - p1.x, &t1, &t2))
		{
			cout << "First two ifs are successful" << endl;
			//dy = p2.y - p1.y;
			if(ClipTest(-dy, p1.y - clip.bottom, &t1, &t2))
				if(ClipTest(dy, clip.top - p1.y, &t1, &t2))
				{
					if (t2 < 1.0) {
						p2.x = p1.x + t2 * dx;
						p2.y = p1.y + t2 * dy;
					}
					if (t1 > 0.0) {
						p1.x += t1 * dx;
						p1.y += t1 * dy;
					}
					cout << "This should draw a line now" << endl;
					LineDDA(screen, p1, p2, WHITE);
			}
		}
}

bool SameSide(POINT2D a, POINT2D b, POINT2D l1, POINT2D l2)
{ //--- l1 and l2 are the ends of the line
  //--- returns 1 if a & b are on the same side of line
	int apt = (a.x - l1.x) * (l2.y - l1.y) - (l2.x - l1.x) * (a.y - l1.y);
	int bpt = (b.x - l1.x) * (l2.y - l1.y) - (l2.x - l1.x) * (b.y - l1.y);
	return ((apt / 1000) * (bpt / 1000) > 0);
}

bool inside(POINT2D p, POINT2D a, POINT2D b, POINT2D c)
{
	return SameSide(p, a, b, c) && SameSide(p, b, a, c) && SameSide(p, c, a, b);
}

//Checks for Concave points within an n sided polygon
bool ConcavePoint(POINT2D p, POINT2D l1, POINT2D l2)
{
	int dot = (p.x - l1.x) * (l2.x - l1.x) + (p.y - l1.y) * (l2.y - l1.y);
	int det = (p.x - l1.x) * (l2.y - l1.y) - (p.y - l1.y) * (l2.x - l1.x);
	return (atan2(det, dot) * 180 / PI) > 0;
}

void NSidedPolygon(BYTE *screen, vector<POINT2D> vertices, vector<RGB> colors)
{
	int index = 0, adjPos1 = 1, adjPos2 = vertices.size() - 1;
	while (vertices.size() > 2 && index != vertices.size()) //to determine if the shape is a polygon/finished all points
	{
		bool passed = true;
		//To find if any point in the polygon is within the current selection
		for (int i = 0; i < vertices.size(); i++)
		{
			if (inside(vertices.at(i), vertices.at(index), vertices.at(adjPos1), vertices.at(adjPos2)) ||
				!ConcavePoint(vertices.at(index), vertices.at(adjPos2), vertices.at(adjPos1)))
			{
				adjPos2 = index;
				index++;
				if (index == vertices.size() - 1)
					adjPos1 = 0;
				else
					adjPos1++;
				passed = false;
				break;
			}
		}
		if (passed)
		{
			//draw triangle
			FilledPolygon(screen, vertices.at(index), vertices.at(adjPos1), vertices.at(adjPos2),
				colors.at(index), colors.at(adjPos1), colors.at(adjPos2));
			vertices.erase(vertices.begin()+index);
			colors.erase(colors.begin()+index);
			index = 0, adjPos1 = 1, adjPos2 = vertices.size() - 1;
		}
	}
}

void BuildFrame(BYTE *pFrame, int view)
{
	BYTE*	screen = (BYTE*)pFrame;		// use copy of screen pointer for safety

	/*POINT2D pos = { 140, 500 };
	POINT2D pos1 = { 700, 300 };
	POINT2D pos2 = { 300, 20 };

	RGB color1 = { 255, 0, 0 };
	RGB color = { 0, 255, 0 };
	RGB color2 = { 0, 0, 255 };*/

	/*for (int i = 0; i < 2; i++)
	{
		POINT2D pos = { rand() % FRAME_WIDE, rand() % FRAME_HIGH };
		POINT2D pos1 = { rand() % FRAME_WIDE, rand() % FRAME_HIGH };
		POINT2D pos2 = { rand() % FRAME_WIDE, rand() % FRAME_HIGH };

		RGB color = { rand() % 256, rand() % 256, rand() % 256 };
		RGB color1 = { rand() % 256, rand() % 256, rand() % 256 };
		RGB color2 = { rand() % 256, rand() % 256, rand() % 256 };
		FilledPolygon(screen, pos, pos1, pos2, color, color1, color2);
	}*/

	/*RECT clip{ 250, 500, 750, 100 };
	POINT2D pos = { 140, 550 };
	POINT2D pos1 = { 800, 50 };
	ClipLine(screen, clip, pos, pos1);*/

	vector<POINT2D> vertices;
	vector<RGB> colors;
	
	/*vertices.push_back(POINT2D{ 190, 330 });
	vertices.push_back(POINT2D{ 200, 220 });
	vertices.push_back(POINT2D{ 340, 270 });
	vertices.push_back(POINT2D{ 400, 390 });
	vertices.push_back(POINT2D{ 275, 350 });
	vertices.push_back(POINT2D{ 150, 470 });*/
	vertices.push_back(POINT2D{ 566, 362 });
	vertices.push_back(POINT2D{ 238, 315 });
	vertices.push_back(POINT2D{ 178, 194 });
	vertices.push_back(POINT2D{ 506, 174 });
	vertices.push_back(POINT2D{ 796, 313 });
	vertices.push_back(POINT2D{ 326, 226 });
	vertices.push_back(POINT2D{ 844, 415 });
	vertices.push_back(POINT2D{ 224, 409 });

	colors.push_back(RED);
	colors.push_back(GREEN);
	colors.push_back(BLUE);
	colors.push_back(RED);
	colors.push_back(GREEN);
	colors.push_back(BLUE);
	colors.push_back(RED);
	colors.push_back(GREEN);

	NSidedPolygon(screen, vertices, colors);

	Sleep(1000);
}
